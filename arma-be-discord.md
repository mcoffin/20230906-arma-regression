`codeyeti @ 07/12/2023 2:52 PM`

> Still the same unfortunately. I wind up at the same oddly bugged UI screen each time. I'll DM you a video of it, but it looks like nigh *all* UI elements I'd usually see (think group chat icon popping up during connect, etc.) all get bunched up about a third of the way in the upper left corner, and she just sits there. Something definitely up with the sigchecking maybe based on RPT log from `v14`. Video of it is uploading rn
> ```
> 14:40:54 Error 5 reading file ''
> 14:46:04 Skipping mods and signatures due to overflow flag being set.
> 14:46:04 Skipping signatures due to overflow flag being set.
> 14:46:04 Skipping mods and signatures due to overflow flag being set.
> 14:46:04 Skipping mods and signatures due to overflow flag being set.
> 14:46:04 Skipping mods and signatures due to overflow flag being set.
> 14:46:04 Skipping mods and signatures due to overflow flag being set.
> 14:46:04 Skipping mods and signatures due to overflow flag being set.
> 14:46:04 Skipping mods and signatures due to overflow flag being set.
> 14:46:04 Skipping mods and signatures due to overflow flag being set.
> 14:46:04 Skipping mods and signatures due to overflow flag being set.
> 14:46:04 Skipping mods and signatures due to overflow flag being set.
> 14:46:04 Skipping mods and signatures due to overflow flag being set.
> 14:46:04 Skipping mods and signatures due to overflow flag being set.
> 14:46:04 Skipping mods and signatures due to overflow flag being set.
> 14:46:04 Skipping mods and signatures due to overflow flag being set.
> 14:46:04 Skipping mods and signatures due to overflow flag being set.
> 14:46:04 Skipping mods and signatures due to overflow flag being set.
> 14:46:04 Skipping mods and signatures due to overflow flag being set.
> 14:46:04 Skipping mods and signatures due to overflow flag being set.
> 14:46:05 Skipping mods and signatures due to overflow flag being set.
> 14:46:05 Skipping mods and signatures due to overflow flag being set.
> 14:46:05 Skipping mods and signatures due to overflow flag being set.
> <this continues on>
> ```

`codeyeti @ 07/19/2023 5:31 AM`

> I'm sure someone else is getting it and it'll get there before release, but quick reminder that I'm still getting the "Skipping sigchecks because overflow bit set" error when trying to join any server w/ sigchecks on the v14 and v15 clients.
> 
> Didn't play long, but things appear to be working nominally when playing w/o sigchecks though, no not quite sure what would do that. lmk what you need if you need info on it.

`codeyeti @ 07/19/2023 6:26 AM`

> I have to leave the sigchecks thing alone for a bit since I have real work, but the only notable thing I found was the folling "Error 5 reading file ''", which rings a bell from *ages* back with either BE or maybe case sensitive file systems? I'll try to remember, but it happens upon arriving @ the home screen, before any apparent issues, and only on problematic versions (currently: `v14` and `v15`). strange, that since data hasn't changed to have something like that pop up if it *actually* is case-sensitive FS, so I'm thinking that's the wrong tree to be barking up, but thought I'd dump my findings before I dipped out for the day.
> 
> ```
> 6:20:53 Reading cached action map data
>  6:20:53 Warning: looped for animation: a3\anims_f_epa\data\anim\sdr\cts\hubcleaned\briefing\hubbriefing_loop.rtm differs (looped now 0)! MoveName: hubbriefing_ext
>  6:20:53 Warning: looped for animation: a3\anims_f_epa\data\anim\sdr\cts\hubcleaned\spectator\hubspectator_stand.rtm differs (looped now 1)! MoveName: hubspectator_stand_contact
>  6:20:53 MovesType CfgMovesMaleSdr load time 538.0 ms
>  6:20:53 Error 5 reading file ''
> ```

`detectivemichealscarn @ 08/06/2023 5:32 AM`


> as in all uppercase or just containing uppercase. if you mean the latter then it's definitely possible
> We have a bunch of missions on the server with uppercase letters. An example of one :
> `Are20we20the20baddies3fV1.Altis.pbo`

`dedmen @ 08/08/2023 11:59PM`

> Got screenshot. Its actual signature error, not steam
> Seems that using the uppercase pbo thing fix, on client breaks signatures 🤔 running v16 on client is fine
> But there should be more people with that problem? 

`dedmen @ 08/08/2023 12:04PM`

> No it's not the steam size that's the problem
> I'll try to reproduce it on thursday

`codeyeti @ 08/11/2023 2:55PM`

> I've still been broken (join any server with sigchecks = just hang on join) since v...15? Whenever the first big sign change was.
> 
> I posted pretty lengthy info with video and logs before,  but if yall need any more information, just let me know

`dedmen @ 08/11/2023 2:56PM`

> can you confirm that v14 is fine? 
> its the cache.ch thing for HC then probably.. If that somehow applies to more than just HC
> 
> Either v14 is fine and its the cache.ch, or v16 is fine and its the lowercase

I somehow missed this last message :( -- hopefully reaching out now can still be useful 🪦
