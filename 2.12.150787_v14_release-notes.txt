2.12.150787 new PROFILING branch with PERFORMANCE binaries, v14, server and client, windows 32/64-bit, linux server 32/64-bit
 - Tweaked: deleteVehicle is now able to delete #track/#mark type objects - https://feedback.bistudio.com/T172751
 - Tweaked: The game date acquisition routine is now more optimized
 - Fixed: When a vehicle is deleted not all tracks were deleted with it - https://feedback.bistudio.com/T172751
 - Fixed: explosionForceCoef was ignored for direct hits
 - Fixed: Vehicles on an empty layer in Eden Editor would be incorrectly assigned to a group if one was created earlier -  https://feedback.bistudio.com/T169079
 - Fixed: Pylon weapons did not trigger "GunParticles" effects
 - Fixed: MusicStop Event Handler did not fire on 'playMusic ""'  - https://feedback.bistudio.com/T173412
 - Fixed: damageEffects material swap did not consider hiddenSelection material changes - https://feedback.bistudio.com/T173561

If you don't want to use the Steam branch, the files are also available for alternative download here:
https://drive.google.com/drive/folders/15p9j7C2nHUt6NoVfChX4YFuqzFXzblJh
