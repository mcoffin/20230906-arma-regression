# ArmA 2.12/2.14 Proton BattlEye Regression

## Bisection log for performance branch 2.12

| Build | Status |
| ----- | ------ |
| `2.12-legacy` | <span style="color:green">✅</span> |
| `v08`/`150657` | <span style="color:green">✅</span> |
| `v12` / `150698` | <span style="color:green">✅</span> |
| `v15` / `150806` | <span style="color:red">❌</span> |
| `v13` / `150715` | <span style="color:green">✅</span> |
| `v14` / `150787` | <span style="color:red">❌</span> |

**FIRST BAD BUILD** - [`212_150787_v14`](https://drive.google.com/drive/folders/1tVDHPi-0BBdt9CGvon0e0Pqeg1OBzFn-)
